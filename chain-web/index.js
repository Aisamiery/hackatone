/**
 * Author: Shabalin Pavel
 * Email: aisamiery@gmail.com
 */

import { rpc, tx } from "@cityofzion/neon-js";

const Neon = require('@cityofzion/neon-js');
const { api, wallet, u } = Neon;


const config = {
  name: "PrivateNet",
  extra: {
    neoscan: "http://95.216.223.119:4000/api/main_net"
  }
};

//const address = 'AK2nJJpJr6o664CWJKi1QRXjqeic2zRp8y';

const privateNet = new rpc.Network(config);
Neon.default.add.network(privateNet);

const account = new wallet.Account('8ac08c36f687daccc5294071167e25a6e9f72e50fa5bb746799dec7d9a1d6031')

//const account = Neon.default.create.account();

console.log(account);

//console.log();




Neon.api.doInvoke({
  net: "PrivateNet",
  api: new Neon.api.neoscan.instance("PrivateNet"),
  account: account,
  //intents: Neon.api.makeIntent({ GAS: 0.00000001 }, account.address),
  script:  Neon.default.create.script({
    scriptHash: 'db80d5580afd2ef7b477dea556bd6ec1973775b5',
    args: [
      u.reverseHex(wallet.getScriptHashFromAddress(account.address)),
      u.str2hexstring('event'),
      u.str2hexstring('object321'),
      u.str2hexstring('test|data|string|to|chain334')
    ]
  })
})
  .then(response => {
    console.log(response.response.txid);
  })


// You will be able to lookup an instance of PrivateNet neoscan
// var privateNetNeoscan = new Neon.api.neoscan.instance("PrivateNet");
// privateNetNeoscan.getBalance(account.address).then(res => console.log(res));

// Neon
//   .default
//   .get
//   .balance('PrivateNet', address)
//   .then(response => {
//     response.assets.GAS.balance;
//     response.assets.NEO.balance;
//   })