/**
 * Author: Shabalin Pavel
 * Email: aisamiery@gmail.com
 */

const path = require('path');

module.exports = {
  mode: "development",
  entry: path.resolve(__dirname, "index.js"),
  output: {
    path: __dirname,
    filename: "script.js"
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        include: [
          path.resolve(__dirname, "src")
        ],
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env']
          }
        }
      }
    ]
  },
  resolve: {
    modules: [
      "node_modules",
      //path.resolve(__dirname, "src")
    ],
    extensions: [".js"]
  },

}