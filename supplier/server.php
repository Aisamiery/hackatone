<?php

defined('_BZEXEC') or die;

class ExeActions extends ExeMain
{
    public function action_getSuppliers()
    {
        $bzExeFuncs = new bzExeFuncs();

        return [
            'res' => 'ok',
            'inf' => [
                'org' => $bzExeFuncs->getMyOrg()
            ],
            'data' => $bzExeFuncs->getSuppliers(),
        ];
    }

}