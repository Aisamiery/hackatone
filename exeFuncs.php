<?php

defined('_BZEXEC') or die;

Class bzExeFuncs
{
    public function getEvents()
    {
        $orgObj = $this->getMyOrg();

        $elemObj = new Element('doc_event');
        $resArr = $elemObj->queryIf('deleted=0 AND userin=:userin', ['userin' => $orgObj->kontragent->id]);

        //sort
        $bzGlUtils = new bzGlUtils();
        $resArr = $bzGlUtils->arrayOrderBy($resArr, 'nomer', SORT_DESC);

        $bzGlStrUtils = new bzGlStrUtils();
        $resArrMod = array();
        foreach ($resArr as $item) {

            $bzGlDateUtils = new bzGlDateUtils($item['data']);
            $item['datafull'] = $bzGlDateUtils->toBzDateTimeString(false);

            $bzGlDateUtils = new bzGlDateUtils($item['startdate']);
            $item['startdatefull'] = $bzGlDateUtils->toBzDateTimeString(false);

            $bzGlDateUtils = new bzGlDateUtils($item['enddate']);
            $item['enddatefull'] = $bzGlDateUtils->toBzDateTimeString(false);

//            $item['cenafull'] = $bzGlStrUtils->dengiFormat($item['cena'], '', false);
//            if ($item['cenafull'] == '0') {
//                $item['cenafull'] = '';
//            }

            $item['statusworkfull'] = [];
            if ($item['statuswork'] > 0) {
                $elemObj = new Element('cat_statuswork');
                $elemObj->findById($item['statuswork']);
                $item['statusworkfull'] = $elemObj->getArray();
            }

            $item['statusofeventfull'] = [];
            if ($item['statusofevent'] > 0) {
                $elemObj = new Element('cat_status');
                $elemObj->findById($item['statusofevent']);
                $item['statusofeventfull'] = $elemObj->getArray();
            }

            $item['userinfull'] = [];
            if ($item['userin'] > 0) {
                $elemObj = new Element('cat_kontragent');
                $elemObj->findById($item['userin']);
                $item['userinfull'] = $elemObj->getArray();
            }

            $item['useroutfull'] = [];
            if ($item['userout'] > 0) {
                $elemObj = new Element('cat_kontragent');
                $elemObj->findById($item['userout']);
                $item['useroutfull'] = $elemObj->getArray();
            }

            $item['hardwarefull'] = [];
            if ($item['hardwareid'] > 0) {
                $elemObj = new Element('cat_hardware');
                $elemObj->findById($item['hardwareid']);
                $item['hardwarefull'] = $elemObj->getArray();
            }

            $resArrMod[] = $item;
        }

        return $resArrMod;
    }

    public function getHardwares()
    {
        $orgObj = $this->getMyOrg();

        $elemObj = new Element('cat_hardware');
        $resArr = $elemObj->queryIf('deleted=0 AND owner=:userin', ['userin' => $orgObj->kontragent->id]);

        //sort
        $bzGlUtils = new bzGlUtils();
        $resArr = $bzGlUtils->arrayOrderBy($resArr, 'name', SORT_DESC);

        $bzGlStrUtils = new bzGlStrUtils();
        $resArrMod = array();
        foreach ($resArr as $item) {

            $bzGlDateUtils = new bzGlDateUtils($item['startdate']);
            $item['startdatefull'] = $bzGlDateUtils->toBzDateTimeString(false);

            $bzGlDateUtils = new bzGlDateUtils($item['enddate']);
            $item['enddatefull'] = $bzGlDateUtils->toBzDateTimeString(false);
            $bzGlDateUtilsNow = new bzGlDateUtils();
            $item['enddatedifffull'] = $bzGlDateUtilsNow->diffInDays($bzGlDateUtils);
            if ($item['enddatedifffull'] < 30) {
                $item['enddatediffclass'] = 'warning';
            } elseif ($item['enddatedifffull'] < 90) {
                $item['enddatediffclass'] = 'primary';
            } else {
                $item['enddatediffclass'] = 'default';
            }

            $item['typefull'] = [];
            if ($item['type'] > 0) {
                $elemObj = new Element('cat_hardwaretype');
                $elemObj->findById($item['type']);
                $item['typefull'] = $elemObj->getArray();
            }

            $item['kontraktfull'] = [];
            if ($item['kontrakt'] > 0) {
                $elemObj = new Element('doc_kontrakt');
                $elemObj->findById($item['kontrakt']);
                $item['kontraktfull'] = $elemObj->getArray();
            }

            $item['manufacturerfull'] = [];
            if ($item['manufacturer'] > 0) {
                $elemObj = new Element('cat_manufacturer');
                $elemObj->findById($item['manufacturer']);
                $item['manufacturerfull'] = $elemObj->getArray();
            }

            $resArrMod[] = $item;
        }

        return $resArrMod;
    }

    public function getSuppliers()
    {
        $orgObj = $this->getMyOrg();

        $elemObj = new Element('doc_kontrakt');
        $resArr = $elemObj->queryIf('deleted=0 AND userin=:userin', ['userin' => $orgObj->kontragent->id]);

        $bzGlStrUtils = new bzGlStrUtils();
        $resArrMod = array();
        foreach ($resArr as $item) {
            if ($item['userout'] > 0) {
                $elemObj = new Element('cat_kontragent');
                $elemObj->findById($item['userout']);
                $kontragent = $elemObj->getArray();
//                $kontragent['director']=$elemObj->getFullObj('director');
            }

            $resArrMod[$item['userout']] = $kontragent;
        }

        //sort
        $bzGlUtils = new bzGlUtils();
        $resArrMod = $bzGlUtils->arrayOrderBy($resArrMod, 'name', SORT_ASC);

        return $resArrMod;
    }

    public function getHistory($elemId = 0)
    {
        $orgObj = $this->getMyOrg();

        $elemObj = new Element('doc_event');
        $resArr = $elemObj->queryIf('deleted=0 AND userin=:userin AND hardwareid=:elemId',
            [
                'userin' => $orgObj->kontragent->id,
                'elemId' => $elemId,
            ]);

        //sort
        $bzGlUtils = new bzGlUtils();
        $resArr = $bzGlUtils->arrayOrderBy($resArr, 'nomer', SORT_DESC);
//
//        $bzGlStrUtils = new bzGlStrUtils();
        $resArrMod = array();
        foreach ($resArr as $item) {

            $bzGlDateUtils = new bzGlDateUtils($item['data']);
            $item['datafull'] = $bzGlDateUtils->toBzDateTimeString(false);

            $bzGlDateUtils = new bzGlDateUtils($item['startdate']);
            $item['startdatefull'] = $bzGlDateUtils->toBzDateTimeString(false);

            $bzGlDateUtils = new bzGlDateUtils($item['enddate']);
            $item['enddatefull'] = $bzGlDateUtils->toBzDateTimeString(false);

//            $item['cenafull'] = $bzGlStrUtils->dengiFormat($item['cena'], '', false);
//            if ($item['cenafull'] == '0') {
//                $item['cenafull'] = '';
//            }

            $item['statusworkfull'] = [];
            if ($item['statuswork'] > 0) {
                $elemObj = new Element('cat_statuswork');
                $elemObj->findById($item['statuswork']);
                $item['statusworkfull'] = $elemObj->getArray();
            }

            $item['statusofeventfull'] = [];
            if ($item['statusofevent'] > 0) {
                $elemObj = new Element('cat_status');
                $elemObj->findById($item['statusofevent']);
                $item['statusofeventfull'] = $elemObj->getArray();
            }

            $item['userinfull'] = [];
            if ($item['userin'] > 0) {
                $elemObj = new Element('cat_kontragent');
                $elemObj->findById($item['userin']);
                $item['userinfull'] = $elemObj->getArray();
            }

            $item['useroutfull'] = [];
            if ($item['userout'] > 0) {
                $elemObj = new Element('cat_kontragent');
                $elemObj->findById($item['userout']);
                $item['useroutfull'] = $elemObj->getArray();
            }

            $item['hardwarefull'] = [];
            if ($item['hardwareid'] > 0) {
                $elemObj = new Element('cat_hardware');
                $elemObj->findById($item['hardwareid']);
                $item['hardwarefull'] = $elemObj->getArray();
            }

            $resArrMod[] = $item;
        }

        return $resArrMod;
    }

    public function getMyOrg()
    {
        $bzSystem = new bzSystem();

        return $bzSystem->user();
    }

}
