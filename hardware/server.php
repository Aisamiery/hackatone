<?php

defined('_BZEXEC') or die;

class ExeActions extends ExeMain
{
    public function action_getHardwares()
    {
        $bzExeFuncs = new bzExeFuncs();

        return [
            'res' => 'ok',
            'inf' => [
                'org' => $bzExeFuncs->getMyOrg()
            ],
            'data' => $bzExeFuncs->getHardwares(),
        ];
    }

    public function action_openHardwareElem()
    {
        $this->cleanID(
            ['objId' => 'trim|sanitize_numbers',],
            ['objId' => 'integer|max_len,50|min_len,1']
        );
        $bzExeFuncs = new bzExeFuncs();

        $hardwareObj = new Element('cat_hardware');
        $hardwareObj->findById($this->ID['objId']);

        return [
            'res' => 'ok',
            'inf' => [
                'hardware' => $hardwareObj->getArray(),
            ],
            'data' => $bzExeFuncs->getHistory($this->ID['objId']),
        ];
    }

}