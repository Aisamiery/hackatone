(function () {
    'use strict';

    var bzExe = {
        type: 'user', //WARNING check the exe type (load from: 'pltfm', 'conf', 'user')
        name: 'tender', //WARNING check the exe name - any name (only letters, example: 'form')
        subName: 'hardware', //WARNING check the exe sub name ('main' - default), any name (only letters, example: 'client')
        nameUC: function () {
            var bzExename = bzExe.name;
            if (bzExename.slice(0, 4) == 'exe_') {
                bzExename = bzExename.slice(4, 50);
            }
            return bzExename.charAt(0).toUpperCase() + bzExename.substr(1).toLowerCase();
        },
        typeUC: function () {
            return bzExe.type.charAt(0).toUpperCase() + bzExe.type.substr(1).toLowerCase();
        },
        subNameUC: function () {
            return bzExe.subName.charAt(0).toUpperCase() + bzExe.subName.substr(1).toLowerCase();
        },
        fullName: function () {
            return 'exe' + bzExe.typeUC() + bzExe.nameUC() + bzExe.subNameUC();
        }
    };

    angular.module(bzExe.fullName() + 'Module', []).controller(bzExe.fullName() + 'Controller', ['$scope', '$filter', '$timeout', '$rootScope', function ($scope, $filter, $timeout, $rootScope) {
        if ($scope.bzConf.debug == 1) {
            console.info('exe: `' + bzExe.fullName() + '` - loaded');
        }

        //init
        $scope.exe = {};
        $scope.openElem = function () {
            $scope.bzPltfmAddTab({
                title: 'Оборудование',
                exe: {name: 'exe_form'},
                obj: {type: 'cat_hardware', id: elem.id,}
            });
        };

        $scope.getHardwares = function () {
            var thisFunctionAction = $scope.bzPltfmGetFunctionName();

            var requestParams = {
                bzGlType: bzExe.type,
                bzGlName: bzExe.name,
                bzGlSubName: bzExe.subName,
                bzGlAction: thisFunctionAction,
            };
            $scope.exe.hardwares = [];
            $scope.exe.loading = true;
            return $scope.bzPltfmRequest(requestParams).then(function successCallback(response) {
                $scope.exe.hardwares = response.data;
                $scope.exe.org = response.inf.org;
                $scope.exe.loading = false;

                return true;
            });
        };

        $scope.openHardwareElem = function (elem) {
            var thisFunctionAction = $scope.bzPltfmGetFunctionName();


            var requestParams = {
                bzGlType: bzExe.type,
                bzGlName: bzExe.name,
                bzGlSubName: bzExe.subName,
                bzGlAction: thisFunctionAction,
                objId: elem.id,
            };
            $scope.exe.history = [];
            $scope.exe.loading = true;
            return $scope.bzPltfmRequest(requestParams).then(function successCallback(response) {
                $scope.exe.history = response.data;
                console.log($scope.exe.history);
                // $scope.exe.org = response.inf.org;
                $scope.exe.loading = false;

                if (elem.active) {
                    elem.active = false;
                } else {
                    elem.active = true;
                }

                return true;
            });
        };

        //LISTEN TO BROADCAST EVENT ************
        // $scope.$on('onElementChange', function(event, element) {
        //     $scope.getHardwares(); //обновить журнал
        // });
        //LISTEN TO BROADCAST EVENT ************

        //AUTOLOAD *****************************
        $scope.getHardwares();

    }]);


})();


