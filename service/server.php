<?php

defined('_BZEXEC') or die;

class ExeActions extends ExeMain
{
    public function action_getEvents()
    {
        $bzExeFuncs = new bzExeFuncs();

        return [
            'res' => 'ok',
            'inf' => [
                'org' => $bzExeFuncs->getMyOrg()
            ],
            'data' => $bzExeFuncs->getEvents(),
        ];
    }

}