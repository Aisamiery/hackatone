from boa.interop.Neo.Storage import Get,Put,Delete,GetContext
from boa.interop.Neo.Runtime import CheckWitness

def Main(addr, operation, objectId, value):

    if not verifiedAddress(addr):
        return False

    ctx = GetContext()

    # add event
    if operation == 'event':
        Put(ctx, objectId, value)
        return value
    # get last event
    elif operation == 'getEvent':
        data = Get(ctx, objectId)
        return data


def verifiedAddress(addr):

    isCaller = CheckWitness(addr)

    if isCaller:
        return True

    return False